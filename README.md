# rpany-pi

An [rpany] mixin for controlling the GPIO pins of a [Raspberry Pi] by way of [wiring pi].

## Usage

Command line tool:

```
sudo rpany-pi
```

The output of the command provides a long list of available `words`, categorised by the
`vocabulary` they belong to. The words can be accessed directly from the command line,
as part of a shebang'd script or by way of an interactive shell.

Interactive shell:

```
$ sudo rpany-pi-shell
> tool-help rpany-pi
pi        : pins-write pwm-bal pwm-ms pwm-range pwm-clock pi-revision wpi-to-gpio phys-to-gpio pad-drive
pin_io    : pin-write pin-write-pwm pin-read analog-read analog-write
pin_setup : pin-out pin-in pin-pwm pin-clock pin-up pin-down pin-off
timing    : since-milli since-micro delay-milli delay-micro
wiring    : wiring-init wiring-gpio wiring-phys wiring-sys
> wiring-init
> 0 pin-out
> 0 1 pin-write
> 0 pin-read .
1
> 0 0 pin-write
> 0 pin-read .
0
> : led 0 ;
> : on 1 pin-write ;
> : off 0 pin-write ;
> led on
> led off
> led on 500 delay-milli led off
> : switch dup pin-read 1 ^ pin-write ;
> led switch led pin-read .
1
> led switch led pin-read .
0
> 10 0 do led switch 0.05 sleep loop
> vocab-see user
: led 0 ;
: on 1 pin-write ;
: off 0 pin-write ;
: switch dup pin-read 1 ^ pin-write ;
>
```

## Prerequisites

* [cmake]
* c++11 compiler
* [rpany]
* [rpany-comms]
* [wiring pi]

## Build

To obtain the dependencies on [raspbian], it is sufficient to run:

```
$ sudo apt-get install build-essential git cmake libboost1.55-all-dev
```

NOTE: My raspbian is old - the boost version may have changed. Just take the
most recent.

You will then need to install [rpany] and [rpany-comms] in that order. The cmake
procedure is the same for each of them - the specifics of rpany-pi are shown here
but it should be enough to simply replace rpany-pi with rpany and rpany-comms to
install the others.

ADVANCED: If you know cmake, feel free to edit the CMakefiles for the other two
projects. Technically, they don't require any compilation at all. Only the header
files they provide in their `include` directory are required, along with the
generated cmake stubs and `lib` contents.

Clone:

```
$ git clone https://gitlab.com/lilo_booter/rpany-pi
```

Configuration:

```
$ cd rpany-pi
$ mkdir out
$ cd out
$ cmake -DCMAKE_CXX_FLAGS="-O3 -Wall" ..
$ cd ..
```

Installation:

```
sudo cmake --build out -- -j2 install
```

## Details

This extends rpany with a GPIO specific vocabulary.

## Vocabularies

### `wiring`

Setup words - one of these must be used before any of the words are used.

| Word          | Usage         | Result | Comment                                            |
| ------------- | ------------- | ------ | -------------------------------------------------- |
| `wiring-init` | `wiring-init` |        | Initialise the gpio system using wiringPiSetup     |
| `wiring-gpio` | `wiring-gpio` |        | Initialise the gpio system using wiringPiSetupGpio |
| `wiring-phys` | `wiring-phys` |        | Initialise the gpio system using wiringPiSetupPhys |
| `wiring-sys`  | `wiring-sys`  |        | Initialise the gpio system using wiringPiSetupSys  |

### `pin_setup`

| Word        | Usage             | Result | Comment                     |
| ----------- | ----------------- | ------ | --------------------------- |
| `pin-out`   | `<pin> pin-out`   |        | Set `pin` as an output      |
| `pin-in`    | `<pin> pin-in`    |        | Set `pin` as an input       |
| `pin-pwm`   | `<pin> pin-pwm`   |        | Set `pin` as a pwm output   |
| `pin-clock` | `<pin> pin-clock` |        | Set `pin` as a clock output |
| `pin-up`    | `<pin> pin-up`    |        | Pull `pin` up               |
| `pin-down`  | `<pin> pin-down`  |        | Pull `pin` down             |
| `pin-off`   | `<pin> pin-off`   |        | Pull `pin` off              |

### `pin_io`

| Word            | Usage                         | Result    | Comment                       |
| --------------- | ----------------------------- | --------- | ----------------------------- |
| `pin-write`     | `<pin> <value> pin-write`     |           | Write `value` to `pin`        |
| `pin-write-pwm` | `<pin> <value> pin-write-pwm` |           | Write `value` to `pin` as pwm |
| `pin-read`      | `<pin> pin-read`              | `<value>` | Read `value` from `pin`       |
| `analog-write`  | `<pin> <value> analog-write`  |           | Write `value` to `pin`        |
| `analog-read`   | `<pin> analog-read`           | `<value>` | Read `value` from `pin`       |

### `timing`

| Word            | Usage                         | Result    | Comment                                  |
| --------------- | ----------------------------- | --------- | ---------------------------------------- |
| `since-milli`   | `since-milli`                 | `<value>` | Milliseconds since last gpio instruction |
| `since-micro`   | `since-micro`                 | `<value>` | Microseconds since last gpio instruction |
| `delay-milli`   | `<value> delay-milli`         |           | Sleep for `value` milliseconds           |
| `delay-micro`   | `<value> delay-micro`         |           | Sleep for `value` microseconds           |

### `pi`

| Word           | Usage                         | Result    | Comment                                                    |
| -------------- | ----------------------------- | --------- | ---------------------------------------------------------- |
| `pins-write`   | `<byte> pins-write`           |           | Set first 8 pins according to the bits in the byte         |
| `pwm-bal`      | `pwm-bal`                     |           | Set the pwm in balanced mode                               |
| `pwm-ms`       | `pwm-ms`                      |           | Set the pwm in mark:space mode                             |
| `pwm-range`    | `pwm-range`                   |           | Sets the range register in the PWM generator               |
| `pwm-clock`    | `pwm-clock`                   |           | Sets the divisor for the PWM clock                         |
| `pi-revision`  | `pi-revision`                 | `<value>` | Returns the board revision of the Raspberry Pi             |
| `wpi-to-gpio`  | `<wpi> wpi-to-gpio`           | `<pin>`   | Returns the BCM_GPIO pin number of the supplied <wpi> pin  |
| `phys-to-gpio` | `<pyhs> phys-to-gpio`         | `<pin>`   | Returns the BCM_GPIO pin number of the supplied <phys> pin |

[Raspberry Pi]: https://www.raspberrypi.org/
[cmake]: https://cmake.org/
[rpany]: https://gitlab.com/lilo_booter/rpany/blob/master/README.md
[rpany-comms]: https://gitlab.com/lilo_booter/rpany-comms/blob/master/README.md
[wiring pi]: http://wiringpi.com/
[raspbian]: https://www.raspberrypi.org/downloads/raspbian/
