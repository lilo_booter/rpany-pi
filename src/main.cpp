#include <rpany/rpany-host.hpp>
#include <rpany/rpany-comms.hpp>
#include <rpany/rpany-pi.hpp>

int main( int argc, char **argv )
{
	// Construct a stack instance
	rpany::stack stack;

	// Register all available vocabs
	rpany::educate( stack );

	// Determine if the terminal is a tty
	const bool tty = isatty( fileno( stdin ) );

	// If no arguments are given, run help and exit
	if ( tty && argc == 1 )
		return rpany::eval( stack, "help" );

	// Parse command line args and exit on failure
	int error = rpany::eval( stack, argc, argv, 1 );

	// If we aren't a tty, evaluate stdin
	if ( error == 0 && !tty )
		error = rpany::eval( stack, "eval-stdin" );

	// Display the stack
	if ( error == 0 && stack.depth( ) )
		error = rpany::eval( stack, "dump." );

	return error;
}
