#pragma once

#include <rpany/rpany.hpp>
#include <wiringPi.h>

namespace rpany_comms {
#include "pi/init.hpp"
#include "pi/gpio.hpp"
}
