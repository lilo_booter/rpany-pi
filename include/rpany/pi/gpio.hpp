#pragma once

inline void wiring_init( rpany::stack &s )
{
	if ( wiringPiSetup( ) == -1 )
		throw rpany::stack_exception( "Unable to initialise the GPIO system" );
}

inline void wiring_gpio( rpany::stack &s )
{
	if ( wiringPiSetupGpio( ) == -1 )
		throw rpany::stack_exception( "Unable to initialise the GPIO system" );
}

inline void wiring_phys( rpany::stack &s )
{
	if ( wiringPiSetupPhys( ) == -1 )
		throw rpany::stack_exception( "Unable to initialise the GPIO system" );
}

inline void wiring_sys( rpany::stack &s )
{
	if ( wiringPiSetupSys( ) == -1 )
		throw rpany::stack_exception( "Unable to initialise the GPIO system" );
}

RPANY_OPEN( wiring, rpany::stack &s )
	s.teach( "wiring-init", wiring_init );
	s.teach( "wiring-gpio", wiring_gpio );
	s.teach( "wiring-phys", wiring_phys );
	s.teach( "wiring-sys", wiring_sys );
RPANY_CLOSE( wiring );

inline void pin_out( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	pinMode( pin, OUTPUT );
}

inline void pin_in( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	pinMode( pin, INPUT );
}

inline void pin_pwm( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	pinMode( pin, PWM_OUTPUT );
}

inline void pin_clock( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	pinMode( pin, PWM_OUTPUT );
}

inline void pin_up( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	pullUpDnControl( pin, PUD_UP );
}

inline void pin_down( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	pullUpDnControl( pin, PUD_DOWN );
}

inline void pin_off( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	pullUpDnControl( pin, PUD_OFF );
}

RPANY_OPEN( pin_setup, rpany::stack &s )
	s.teach( "pin-out", pin_out );
	s.teach( "pin-in", pin_in );
	s.teach( "pin-pwm", pin_pwm );
	s.teach( "pin-clock", pin_clock );
	s.teach( "pin-up", pin_up );
	s.teach( "pin-down", pin_down );
	s.teach( "pin-off", pin_off );
RPANY_CLOSE( pin_setup );

inline void pin_write( rpany::stack &s )
{
	auto value = s.pull< bool >( );
	auto pin = s.pull< int >( );
	digitalWrite( pin, value );
}

inline void pin_write_pwm( rpany::stack &s )
{
	auto value = s.pull< int >( );
	auto pin = s.pull< int >( );
	digitalWrite( pin, value );
}

inline void pin_read( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	s += digitalRead( pin );
}

inline void analog_write( rpany::stack &s )
{
	auto value = s.pull< int >( );
	auto pin = s.pull< int >( );
	analogWrite( pin, value );
}

inline void analog_read( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	s += analogRead( pin );
}

RPANY_OPEN( pin_io, rpany::stack &s )
	s.teach( "pin-write", pin_write );
	s.teach( "pin-write-pwm", pin_write_pwm );
	s.teach( "pin-read", pin_read );
	s.teach( "analog-read", analog_read );
	s.teach( "analog-write", analog_write );
RPANY_CLOSE( pin_io );

inline void pins_write( rpany::stack &s )
{
	auto value = s.pull< int >( );
	digitalWriteByte( value & 0xff );
}

inline void pwm_bal( rpany::stack &s )
{
	pwmSetMode( PWM_MODE_BAL );
}

inline void pwm_ms( rpany::stack &s )
{
	pwmSetMode( PWM_MODE_MS );
}

inline void pwm_range( rpany::stack &s )
{
	auto value = s.pull< size_t >( );
	pwmSetRange( value );
}

inline void pwm_clock( rpany::stack &s )
{
	auto value = s.pull< int >( );
	pwmSetClock( value );
}

inline void wpi_to_gpio( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	wpiPinToGpio( pin );
}

inline void phys_to_gpio( rpany::stack &s )
{
	auto pin = s.pull< int >( );
	physPinToGpio( pin );
}

inline void pad_drive( rpany::stack &s )
{
	auto value = s.pull< int >( );
	auto group = s.pull< int >( );
	setPadDrive( group, value );
}

RPANY_OPEN( pi, rpany::stack &s )
	s.teach( "pins-write", pins_write );
	s.teach( "pwm-bal", pwm_bal );
	s.teach( "pwm-ms", pwm_ms );
	s.teach( "pwm-range", pwm_range );
	s.teach( "pwm-clock", pwm_clock );
	s.teach( "pi-revision", [] ( rpany::stack &s ) { s += piBoardRev( ); } );
	s.teach( "wpi-to-gpio", wpi_to_gpio );
	s.teach( "phys-to-gpio", phys_to_gpio );
	s.teach( "pad-drive", pad_drive );
RPANY_CLOSE( pi );

RPANY_OPEN( timing, rpany::stack &s )
	s.teach( "since-milli", [] ( rpany::stack &s ) { s += millis( ); } );
	s.teach( "since-micro", [] ( rpany::stack &s ) { s += micros( ); } );
	s.teach( "delay-milli", [] ( rpany::stack &s ) { delay( s.pull< size_t >( ) ); } );
	s.teach( "delay-micro", [] ( rpany::stack &s ) { delayMicroseconds( s.pull< size_t >( ) ); } );
RPANY_CLOSE( timing );
